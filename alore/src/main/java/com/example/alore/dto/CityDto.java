package com.example.alore.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CityDto {
    private Long cityId;
    private String name;
    private Long stateId;
}

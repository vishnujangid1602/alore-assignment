package com.example.alore.enums;

public enum GenderType {
    MALE, FEMALE, TRANSGENDER
}

package com.example.alore.service;

import com.example.alore.dao.AddressDao;
import com.example.alore.dao.HotelDao;
import com.example.alore.dao.HotelFeedbackDao;
import com.example.alore.dto.HotelDto;
import com.example.alore.dto.HotelFeedbackDataDto;
import com.example.alore.dto.HotelFeedbackDto;
import com.example.alore.entity.AddressEntity;
import com.example.alore.entity.HotelEntity;
import com.example.alore.entity.HotelFeedbackEntity;
import com.example.alore.enums.AddressRefType;
import com.example.alore.form.HotelFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
@Slf4j
public class HotelService {

    @Autowired
    private HotelDao hotelDao;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AddressDao addressDao;

    @Autowired
    private HotelFeedbackDao hotelFeedbackDao;

    @Autowired
    private EntityManager entityManager;

    /**
     * function will add new hotel and it's information and also its address and can also edting all crossponding information.
     * @param hotelDto
     * @throws Exception
     */
    public void addOrUpdateHotel(HotelDto hotelDto) throws Exception{
        HotelEntity hotelEntity;
        if (hotelDto.getHotelId() != null) {
            hotelEntity = hotelDao.findById(hotelDto.getHotelId()).orElseThrow(() -> new Exception("Hotel Not Found"));
        } else {
            hotelEntity = new HotelEntity();
        }

        hotelEntity.setHotelName(hotelDto.getHotelName());
        hotelEntity.setTotalNumberOfRooms(hotelDto.getTotalNumberOfRooms());
        hotelEntity.setHotelStar(hotelDto.getHotelStar());
        hotelEntity.setIsAcFacility(hotelDto.getIsAcFacility());
        hotelEntity.setIsWifiFacility(hotelDto.getIsWifiFacility());
        hotelEntity.setIsRestaurantAvailable(hotelDto.getIsRestaurantAvailable());
        hotelEntity.setCostPerRoom(hotelDto.getCostPerRoom());
        hotelEntity = hotelDao.save(hotelEntity);

        customerService.saveAddress(hotelDto.getAddressDto(), hotelEntity.getId(), AddressRefType.HOTEL);
    }

    /**
     * function will delete the hotel information and also delete is crossponding address from customer address
     * @param hotelId
     * @throws Exception
     */
    public void deleteHotel(Long hotelId) throws Exception {
        HotelEntity hotelEntity = hotelDao.findById(hotelId).orElseThrow(() -> new Exception("Hotel Not Found"));
        hotelDao.delete(hotelEntity);
        AddressEntity addressEntity = addressDao.findByRefIdAndAddressType(hotelId, AddressRefType.HOTEL);
        if (addressEntity != null)
            addressDao.delete(addressEntity);
    }

    /**
     * function will save the user feedback and get the avgerage of rating for that particular hotel and update it to hotel entity.
     * @param hotelFeedbackDto
     */
    public void hotelFeedBack(HotelFeedbackDto hotelFeedbackDto) throws Exception {
        HotelFeedbackEntity hotelFeedbackEntity = hotelFeedbackDao.findByFeedbackByAndFeedbackTo(hotelFeedbackDto.getFeedbackBy(), hotelFeedbackDto.getFeedbackTo());

        if (hotelFeedbackEntity == null) {
            hotelFeedbackEntity = new HotelFeedbackEntity();
        }
        hotelFeedbackEntity.setFeedbackBy(hotelFeedbackDto.getFeedbackBy());
        hotelFeedbackEntity.setFeedbackTo(hotelFeedbackDto.getFeedbackTo());
        hotelFeedbackEntity.setComment(hotelFeedbackDto.getComment());
        hotelFeedbackEntity.setRating(hotelFeedbackDto.getRating());
        hotelFeedbackEntity = hotelFeedbackDao.save(hotelFeedbackEntity);
        float avgRating = hotelFeedbackDao.avgRatingToHotel(hotelFeedbackEntity.getFeedbackTo());

        HotelEntity hotelEntity = hotelDao.findById(hotelFeedbackEntity.getFeedbackTo()).orElseThrow(() -> new Exception("Hotel Not Found"));
        hotelEntity.setHotelRating(avgRating);
        hotelDao.save(hotelEntity);

    }

    /**
     * function ll delete the review and recalculate the avg rating for the hotel
     * @param reviewId
     */
    public void deleteReview(Long reviewId) throws Exception {
        HotelFeedbackEntity hotelFeedbackEntity = hotelFeedbackDao.findById(reviewId).orElseThrow(() -> new Exception("Feedback not found"));
        hotelFeedbackDao.delete(hotelFeedbackEntity);
        HotelEntity hotelEntity = hotelDao.findById(hotelFeedbackEntity.getFeedbackTo()).orElseThrow(() ->new Exception("Hotel not found"));
        hotelEntity.setHotelRating(hotelFeedbackDao.avgRatingToHotel(hotelFeedbackEntity.getFeedbackTo()));
        hotelDao.save(hotelEntity);

    }

    public List<HotelFeedbackDataDto> getHotelreviews(Long hotelId) {
       return hotelFeedbackDao.getHotelFeedback(hotelId);
    }

    /**
     * this function ll provide filtered data of hotel based on the facilities and by the city, assuming for now that all hotels rooms are available for every date;
     * @param filter
     * @return
     */
    public List<HotelDto> searchHotel(HotelFilter filter) {
        TypedQuery<HotelDto> cq = entityManager.createQuery("select new com.example.alore.dto.HotelDto(hE.id, hE.hotelName, hE.hotelRating,hE.hotelStar, hE.isAcFacility, hE.isWifiFacility, hE.isRestaurantAvailable, hE.costPerRoom) from HotelEntity hE inner  join AddressEntity cAE on cAE.refId = hE.id where cAE.addressType = com.example.alore.enums.AddressRefType.HOTEL and cAE.cityId = :cityId and hE.isAcFacility = :isAcFacility and hE.isWifiFacility = :isWifiFacility and hE.isRestaurantAvailable = :isRestaurantAvailable and hE.costPerRoom >= :costPerRoomFrom and hE.costPerRoom <= :costPerRoomTo   order by  hE.hotelRating desc , hE.costPerRoom", HotelDto.class);
        cq.setParameter("cityId", filter.getCityId());
        cq.setParameter("isAcFacility", filter.getIsAcFacility());
        cq.setParameter("isWifiFacility", filter.getIsWifiFacility());
        cq.setParameter("isRestaurantAvailable", filter.getIsRestaurantAvailable());
        cq.setParameter("costPerRoomFrom", filter.getCostPerRoomFrom());
        cq.setParameter("costPerRoomTo", filter.getCostPerRoomTo());

        return cq.getResultList();

    }


}

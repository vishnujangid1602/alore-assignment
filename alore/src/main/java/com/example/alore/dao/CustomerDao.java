package com.example.alore.dao;

import com.example.alore.dto.CustomerDto;
import com.example.alore.entity.CustomerEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerDao extends CrudRepository<CustomerEntity, Long> {
    /*SELECT c.id, c.`name`, ct.`name` FROM alore.customer c left join alore.customer_address cA on c.id = cA.customer_id
left join cities ct on ct.id = cA.city_id;*/
//    Long userId, String name, String email, String password, String mobileNumber, GenderType gender, AddressDto address
    @Query(value = "select  new com.example.alore.dto.CustomerDto(cE.id, cE.name, cE.email, cE.mobileNumber, cE.gender, city.id, city.name) from CustomerEntity cE left join AddressEntity aE on cE.id = aE.refId left join CityEntity city on city.id = aE.cityId where aE.addressType = com.example.alore.enums.AddressRefType.CUSTOMER")
    List<CustomerDto> getAllCustomer();
}

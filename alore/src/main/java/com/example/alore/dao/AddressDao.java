package com.example.alore.dao;

import com.example.alore.entity.AddressEntity;
import com.example.alore.enums.AddressRefType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressDao extends CrudRepository<AddressEntity, Long> {
    AddressEntity findByRefIdAndAddressType(Long customerId, AddressRefType addressRefType);
}

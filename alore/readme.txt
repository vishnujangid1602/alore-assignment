I am assuming that system has Java and Mysql preinstalled. I have used IntelliJ IDEA as IDE and MySql Database.

1. Run all the MySql queries from db_mirgration/migration.sql file in MySQL shell.
2. To connect with DB updated application.properties with username = <your user name for db> and password = <your password> In this case username = root, password = Vishnu612@.


Database Schema Descriptions:

Common fields of all table : is_enabled(for soft delete), updated_at(shows the last updated), created_at(when the row or entry is created).

1. customer : This table will store all related information of the user like it's name, email address, password(encrypt, for now it's just plan-text), gender etc.

2. cities : This table will store all the cities or provided by us.

3. hotel : This table will store the general information about the hotel like it's name, star, rating, cost per room, total number of room etc.

4. address : this table will store address of both hotel and customer like it's city id and refId which is the id of either of cutomer or hotel and this is differ by

	    addressType enum(HOTEL, CUSTOMER) which tells us is the row is related to customer or hotel.

5.hotel_feedback : This table will store the information of the feedback given by the customer to the hotel. In this table we store customerId as feedbackBy, and hotelId as 			  feedbackTo, and rating given by the customer to the hotel.  


Api descriptions : 

POST API'S 

1. /api/customer/create-update-user
  RequestBody

	 {
	    "data" : { 
	     "userId" : 5,   
	    "name" : "test",
	    "email"  : "vishnujangid612@gmail.com",
	    "mobileNumber"   : "7014007036",
	    "address"  : "Jaipur", 
	    "password" : "vishnu123",
	    "address" : {
		"cityName" : "Pune"
	      }
    	   }
	}

2. /api/customer/add-update-city
   RequestBody 
	 {
	    "data" : { 
	    "name" : "Kota"
	    }
	}

3. /api/customer/delete-customer/<customerId>

4 /api/hotel/create-update-hotel
  RequestBody 
	{
	    "data" : { 
	    "hotelName" : "Hotel",
	    "totalNumberOfRooms"  : 100,
	    "hotelStar"   : 5,
	    "isAcFacility"  : true, 
	    "isWifiFacility" : true,
	     "isRestaurantAvailable" : true,
	     "costPerRoom" : 1500,
	     "addressDto" : {
		 "cityName" : "Jaipur"
	     }
	    }
	}

5./api/hotel/delete-hotel/<hotelId>

6. /api/hotel/feedback-hotel
  RequestBody 
	
	{
	    "data" : { 
	     "feedbackBy" : 1,
	      "feedbackTo" : 1,
	       "comment" : "Good",
		 "rating" : 4
	    }
	}

7. /api/hotel/search-hotel
	RequestBody

	{
	    "data" : { 
		"cityId" : 1,
	       "numberOfRoomRequired" : 2,
	        "isAcFacility" : true,
		"isWifiFacility" : true,
		 "isRestaurantAvailable" : true,
		 "costPerRoomFrom" : 0,
		 "costPerRoomTo" :1000
	    }
	}

8. /api/hotel/delete-review/<reviewId>



GET API'S

/api/customer/get-all-customers

/api/hotel/get-hotel-review/<hotelId>

	   

   


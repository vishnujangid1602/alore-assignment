package com.example.alore.entity;

import com.example.alore.entity.base.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "hotel_feedback")
@SQLDelete(sql = "UPDATE customer_address SET is_enabled = false WHERE id = ?")
@Where(clause = "is_enabled = true")
public class HotelFeedbackEntity extends BaseEntity {
    private Long feedbackBy;
    private Long feedbackTo;
    private String comment;
    private Integer rating;
}

package com.example.alore.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HotelFeedbackDataDto {
    private String customerName;
    private String hotelName;
    private Integer ratingGiven;
    private String comment;

    public HotelFeedbackDataDto(String customerName, String hotelName, Integer ratingGiven, String comment) {
        this.customerName = customerName;
        this.hotelName = hotelName;
        this.ratingGiven = ratingGiven;
        this.comment = comment;
    }
}

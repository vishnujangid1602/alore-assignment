package com.example.alore.dto;

import com.example.alore.enums.AddressRefType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressDto {
    private Long cityId;
    private String cityName;
    private Long customerId;
    @Enumerated(EnumType.STRING)
    private AddressRefType addressRefType;
    public AddressDto(Long cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }
}

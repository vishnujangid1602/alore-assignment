package com.example.alore.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HotelFilter {
    private Long cityId;
    private Float hotelRating;
    private Date bookingDate;
    private Integer numberOfRoomRequired;
    private Float hotelStar;
    private Boolean isAcFacility = false;
    private Boolean isWifiFacility = false;
    private Boolean isRestaurantAvailable = true;

    /*cost range default*/
    private Float costPerRoomFrom = 0f;
    private Float costPerRoomTo = 200f;
}

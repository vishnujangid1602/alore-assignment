package com.example.alore.controller;


import com.example.alore.dto.*;
import com.example.alore.form.HotelFilter;
import com.example.alore.service.HotelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/hotel")
@Slf4j
public class HotelController
{

    @Autowired
    private HotelService hotelService;

    @RequestMapping(value = "/create-update-hotel", method = RequestMethod.POST)
    public ResponseWrapper addOrUpdatehotel(@RequestBody RequestWrapper<HotelDto> requestWrapper) throws Exception {
        hotelService.addOrUpdateHotel(requestWrapper.getData());
        return new ResponseWrapper(true, HttpStatus.OK.value(),  new HashMap<>());
    }

    @RequestMapping(value = "/delete-hotel/{hotelId}", method = RequestMethod.POST)
    public ResponseWrapper registerLender(@PathVariable (name = "hotelId") Long hotelId) throws Exception {
        hotelService.deleteHotel(hotelId);
        return new ResponseWrapper(true, HttpStatus.OK.value(),  new HashMap<>());
    }

    @RequestMapping(value = "/feedback-hotel", method = RequestMethod.POST)
    public ResponseWrapper feedBackHotel(@RequestBody RequestWrapper<HotelFeedbackDto> requestWrapper) throws Exception {
        hotelService.hotelFeedBack(requestWrapper.getData());
        return new ResponseWrapper(true, HttpStatus.OK.value(),  new HashMap<>());
    }

    @RequestMapping(value = "/search-hotel", method = RequestMethod.POST)
    public ResponseWrapper searchHotel(@RequestBody RequestWrapper<HotelFilter> requestWrapper) {
        HashMap<String, Object> reponse = new HashMap<>();
        reponse.put("filterApplied",requestWrapper.getData());
        reponse.put("hotels",hotelService.searchHotel(requestWrapper.getData()) );
        return new ResponseWrapper(true, HttpStatus.OK.value(),  reponse);
    }

    @RequestMapping(value = "/get-hotel-review/{hotelId}", method =  RequestMethod.GET)
    public ResponseWrapper getHotelReview(@PathVariable(name = "hotelId") Long hotelId) {

        HashMap<String, Object> reponse = new HashMap<>();
        reponse.put("reviews",   hotelService.getHotelreviews(hotelId));
        return new ResponseWrapper(true, HttpStatus.OK.value(),reponse);
    }

    @RequestMapping(value = "/delete-review/{reviewId}", method =  RequestMethod.POST)
    public ResponseWrapper deleteReview(@PathVariable(name = "reviewId") Long reviewId) throws Exception {
        hotelService.deleteReview(reviewId);
        return new ResponseWrapper(true, HttpStatus.OK.value(),  new HashMap<>());
    }

}

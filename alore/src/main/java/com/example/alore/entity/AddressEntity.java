package com.example.alore.entity;

import com.example.alore.entity.base.BaseEntity;
import com.example.alore.enums.AddressRefType;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "address")
@SQLDelete(sql = "UPDATE address SET is_enabled = false WHERE id = ?")
@Where(clause = "is_enabled = true")
public class AddressEntity extends BaseEntity {
    private Long cityId;
    private Long refId;

    @Enumerated(EnumType.STRING)
    private AddressRefType addressType;
}

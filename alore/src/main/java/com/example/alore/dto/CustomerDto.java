package com.example.alore.dto;

import com.example.alore.enums.GenderType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerDto {
    private Long userId;
    private String name;
    private String email;
    private String password;
    private String mobileNumber;
    @Enumerated(EnumType.STRING)
    private GenderType gender;
    private AddressDto address;

    public CustomerDto(Long userId, String name, String email, String mobileNumber, GenderType gender, Long cityId, String cityName) {
        this.userId = userId;
        this.name = name;
        this.email = email;

        this.mobileNumber = mobileNumber;
        this.gender = gender;
        this.address = new AddressDto(cityId, cityName);
    }
}

package com.example.alore.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HotelDto {
    private Long hotelId;
    private String hotelName;
    private Long cityId;
    private Float hotelRating;
    private Integer totalNumberOfRooms;
    private Integer hotelStar;
    private Boolean isAcFacility;
    private Boolean isWifiFacility;
    private Boolean isRestaurantAvailable;
    private Float costPerRoom;
    private AddressDto addressDto;

    public HotelDto(Long hotelId, String hotelName, Float hotelRating, Integer hotelStar, Boolean isAcFacility, Boolean isWifiFacility, Boolean isRestaurantAvailable, Float costPerRoom) {
        this.hotelId = hotelId;
        this.hotelName = hotelName;
        this.hotelRating = hotelRating;
        this.hotelStar = hotelStar;
        this.isAcFacility = isAcFacility;
        this.isWifiFacility = isWifiFacility;
        this.isRestaurantAvailable = isRestaurantAvailable;
        this.costPerRoom = costPerRoom;
//        this.address = address;
    }

}

package com.example.alore.entity;

import com.example.alore.entity.base.BaseEntity;
import com.example.alore.enums.GenderType;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "customer")
@SQLDelete(sql = "UPDATE customer SET is_enabled = false WHERE id = ?")
@Where(clause = "is_enabled = true")
public class CustomerEntity extends BaseEntity {
    private String name;
    private String email;
    private String password;

    @Enumerated(EnumType.STRING)
    private GenderType gender;

    private String mobileNumber;
}

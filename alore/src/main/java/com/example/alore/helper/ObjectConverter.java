package com.example.alore.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ObjectConverter {

    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss SSS").create();

    public static <T> T convertObject(Object convertFrom,Class<T> toClass){
        try{
            return gson.fromJson(gson.toJson(convertFrom),toClass);
        }catch (Exception e){
            log.warn("Exception in object conversion",e);
        }
        return null;
    }

    public static <T> T convertObject(Object convertFrom, TypeToken<T> toTypeToken){
        try{

            return gson.fromJson(gson.toJson(convertFrom),toTypeToken.getType());
        }catch (Exception e){
            log.warn("Exception in object conversion",e);
        }
        return null;
    }
}

package com.example.alore.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HotelFeedbackDto {
    private Long feedbackBy;
    private Long feedbackTo;
    private String comment;
    private Integer rating;
}

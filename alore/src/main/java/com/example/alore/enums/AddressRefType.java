package com.example.alore.enums;

public enum AddressRefType {
    HOTEL, CUSTOMER;
}

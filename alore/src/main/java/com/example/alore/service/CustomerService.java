package com.example.alore.service;

import com.example.alore.dao.CityDao;
import com.example.alore.dao.AddressDao;
import com.example.alore.dao.CustomerDao;
import com.example.alore.dto.AddressDto;
import com.example.alore.dto.CityDto;
import com.example.alore.dto.CustomerDto;
import com.example.alore.entity.CityEntity;
import com.example.alore.entity.AddressEntity;
import com.example.alore.entity.CustomerEntity;
import com.example.alore.enums.AddressRefType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private CityDao cityDao;

    @Autowired
    private AddressDao addressDao;

    /**
     * function will add or update user.
     * @param customerDto
     * @throws Exception
     */
    public void saveOrUpdateUserInformation(CustomerDto customerDto) throws Exception {
        CustomerEntity customerEntity;
        if (customerDto.getUserId() != null)
            customerEntity = customerDao.findById(customerDto.getUserId()).orElseThrow(() -> new Exception("User Not Found"));
        else customerEntity = new CustomerEntity();

        customerEntity.setName(customerDto.getName());
        customerEntity.setEmail(customerDto.getEmail());
        customerEntity.setMobileNumber(customerDto.getMobileNumber());
        customerEntity.setPassword(customerDto.getPassword());
        customerEntity.setGender(customerDto.getGender());
        customerEntity = customerDao.save(customerEntity);
        saveAddress(customerDto.getAddress(), customerEntity.getId(), AddressRefType.CUSTOMER);
    }

    /**
     * function will add or update city.
     * @param cityDto
     * @throws Exception
     */
    public void addOrUpdateCity(CityDto cityDto) throws  Exception{
        CityEntity cityEntity;
        if (cityDto.getCityId() != null)
            cityEntity = cityDao.findById(cityDto.getCityId()).orElseThrow(() -> new Exception("city not found"));
        else cityEntity = new CityEntity();

        cityEntity.setName(cityDto.getName());
        cityEntity.setStateId(cityDto.getStateId());
        cityDao.save(cityEntity);
    }

    public List<CustomerDto> getAllCustomers() {
        return customerDao.getAllCustomer();
    }

    /**
     * function remove customer and its crossponding address also.
     * @param customerId
     * @param addressRefType
     * @throws Exception
     */
    public void removeCustomer(Long customerId, AddressRefType addressRefType) throws Exception{
        CustomerEntity customerEntity = customerDao.findById(customerId).orElseThrow(() -> new Exception("Customer Not Found"));
        customerDao.delete(customerEntity);
        AddressEntity addressEntity = addressDao.findByRefIdAndAddressType(customerId, addressRefType);
        if (addressEntity != null) {
            addressDao.delete(addressEntity);
        }
    }

    public void saveAddress(AddressDto addressDto, Long customerOrHotelId, AddressRefType addressRefType) {
        if (addressDto!= null  && addressDto.getCityName() != null) {

            /*Considering City of the customer will never null*/
            CityEntity cityEntity = cityDao.findByName(addressDto.getCityName());
            AddressEntity addressEntity = addressDao.findByRefIdAndAddressType(customerOrHotelId,addressRefType);

            if (addressEntity == null) {
                addressEntity = new AddressEntity();
            }

            if (!cityEntity.getId().equals(addressEntity.getCityId())) {



                addressEntity.setAddressType(addressRefType);
                addressEntity.setCityId(cityEntity.getId());
                addressEntity.setRefId(customerOrHotelId);
                addressDao.save(addressEntity);
            }
         }
    }
}

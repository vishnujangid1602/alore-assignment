package com.example.alore.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseWrapper {

	@JsonProperty("success")
	private boolean success;

	@JsonProperty("message")
	private String message;

	@JsonProperty("code")
	private Integer code;

	@JsonProperty("data")
	private Map<String, Object> data;

	public ResponseWrapper(boolean success, Integer code, String message) {
		this.success = success;
		this.code = code;
		this.message = message;
	}

	public ResponseWrapper(boolean success, Integer code, Map<String, Object> data) {
		this.success = success;
		this.code = code;
		this.data = data;
	}

	public ResponseWrapper(boolean success, Map<String, Object> data) {
		this.success = success;
		this.data = data;
	}

	public ResponseWrapper(boolean success, String message, Map<String, Object> data) {
		this.success = success;
		this.data = data;
		this.code = success ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value();
		this.message = message;
	}

	// used for success responses with special success code (other than 200)
	public ResponseWrapper(boolean success, String message, Integer code, Map<String, Object> data) {
		this.success = success;
		this.data = data;
		this.code = code;
		this.message = message;
	}


}

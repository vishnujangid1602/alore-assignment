package com.example.alore.controller;


import com.example.alore.dto.*;
import com.example.alore.enums.AddressRefType;
import com.example.alore.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/customer")
@Slf4j
public class CustomerController {

    @Autowired
    private CustomerService customerService;

        @RequestMapping(value = "/create-update-user", method = RequestMethod.POST)
        public ResponseWrapper registerLender(@RequestBody RequestWrapper<CustomerDto> requestWrapper) throws Exception {
            customerService.saveOrUpdateUserInformation(requestWrapper.getData());
            return new ResponseWrapper(true, HttpStatus.OK.value(),  new HashMap<>());
        }

        @RequestMapping(value = "/add-update-city", method = RequestMethod.POST)
        public ResponseWrapper addCity(@RequestBody RequestWrapper<CityDto> requestWrapper) throws Exception {
            customerService.addOrUpdateCity(requestWrapper.getData());
            return new ResponseWrapper(true, HttpStatus.OK.value(),  new HashMap<>());
        }


        @RequestMapping(value = "/get-all-customers", method = RequestMethod.GET)
        public ResponseWrapper getAllCustomers() {

            HashMap<String, Object> response = new HashMap<>();
            response.put("customers",customerService.getAllCustomers());
            return new ResponseWrapper(true, HttpStatus.OK.value(),response);
        }

    @RequestMapping(value = "/delete-customer/{customerId}", method = RequestMethod.POST)
    public ResponseWrapper removeCustomer(@PathVariable(name = "customerId") Long customerId) throws Exception {
        customerService.removeCustomer(customerId, AddressRefType.CUSTOMER);
        return new ResponseWrapper(true, HttpStatus.OK.value(),  new HashMap<>());
    }


}

package com.example.alore.entity;


import com.example.alore.entity.base.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "hotel")
@SQLDelete(sql = "UPDATE hotel SET is_enabled = false WHERE id = ?")
@Where(clause = "is_enabled = true")
public class HotelEntity extends BaseEntity {
    private String hotelName;
    private Float hotelRating;
    private Integer totalNumberOfRooms;
    private Integer hotelStar;
    private Boolean isAcFacility;
    private Boolean isWifiFacility;
    private Boolean isRestaurantAvailable;
    private Float costPerRoom;
}

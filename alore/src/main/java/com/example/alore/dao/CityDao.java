package com.example.alore.dao;

import com.example.alore.entity.CityEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityDao extends CrudRepository<CityEntity, Long> {
    CityEntity findByName(String cityName);
}

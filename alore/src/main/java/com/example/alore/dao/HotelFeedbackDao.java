package com.example.alore.dao;

import com.example.alore.dto.HotelFeedbackDataDto;
import com.example.alore.entity.HotelFeedbackEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotelFeedbackDao extends CrudRepository<HotelFeedbackEntity, Long> {
    HotelFeedbackEntity findByFeedbackByAndFeedbackTo(Long feedbackBy, Long feedbackTo);

    @Query(value = "select avg (hFE.rating) from HotelFeedbackEntity hFE where hFE.feedbackTo =?1")
    float avgRatingToHotel(Long feedbackTo);

    @Query(value = "select new com.example.alore.dto.HotelFeedbackDataDto(cE.name, hE.hotelName, hFE.rating, hFE.comment) from HotelFeedbackEntity hFE inner  join HotelEntity hE on hFE.feedbackTo = hE.id inner join CustomerEntity cE on hFE.feedbackBy = cE.id where hE.id = ?1")
    List<HotelFeedbackDataDto> getHotelFeedback(Long hotelId);
}

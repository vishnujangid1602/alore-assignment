package com.example.alore.dao;

import com.example.alore.entity.HotelEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HotelDao extends CrudRepository<HotelEntity, Long> {
}

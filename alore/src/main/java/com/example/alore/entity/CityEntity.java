package com.example.alore.entity;

import com.example.alore.entity.base.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "cities")
@SQLDelete(sql = "UPDATE cities SET is_enabled = false WHERE id = ?")
@Where(clause = "is_enabled = true")
public class CityEntity extends BaseEntity {
    private String name;
    private Long stateId;
}
